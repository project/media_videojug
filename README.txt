Media: Videojug

This adds support for the Videojug video sharing service, available at http://www.videojug.com/

To use this module, you'll first need to install Embedded Video Field, which is
packaged with Embedded Media Field (from http://drupal.org/project/emfield).

Set up a content type to use a Third Party Video field as you normally would with emfield.
Also ensure that you have enabled the new Videojug provider from the Admin screen at /admin/content/emfield.
